# Plugin Météo

Ce plugin WordPress, nommé "Météo", affiche la météo des trois prochains jours pour une ville entrée par un utilisateur. Il utilise une API pour récupérer les données météorologiques.

## Caractéristiques

- **Nom du plugin :** meteo
- **Auteur :** Andrianaina Rabenjamina
- **Version :** 1.0.0
- **Licence :** GPL v2 ou plus tard

## Fonctionnalités

- **Styles Bootstrap :** Le plugin charge les styles Bootstrap à partir du CDN.
- **Scripts Bootstrap :** Le plugin charge également les scripts Bootstrap à partir du CDN.
- **CSS personnalisé :** Le plugin charge un fichier CSS personnalisé à partir du répertoire du plugin.
- **JS personnalisé :** De même, le plugin charge un fichier JavaScript personnalisé à partir du répertoire du plugin.
- **Shortcode :** Le plugin fournit un shortcode (`meteo-RABENJAMINA`) qui peut être utilisé pour afficher le formulaire de météo.

## Utilisation

Pour utiliser ce plugin, installez-le et activez-le dans votre tableau de bord WordPress. Ensuite, utilisez le shortcode `[meteo-RABENJAMINA]` dans vos publications ou pages pour afficher le formulaire de météo.